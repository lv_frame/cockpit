# 安厨驾驶舱专用开发模式

## 1 Decorator
### 1.1 简介
驾驶舱主要的模式主要是读取数据，要么从数仓中读取，要么从数据库中读取数据，然后给前端展示，因此不太适合使用具备数据操作的restful api风格的开发模式（太过累赘），为了精简开发，使用Decorator装饰器模式封装常用的数据处理模块，从而开发这个依赖包。

### 1.2 如何安装
```shell
composer require hzanchu/cockpit
```

### 1.3 配置
`.env`文件需要配置如下参数：
```shell
# Logics命名空间，你的数据处理逻辑存放地点
COCKPIT_LOGICS_NAMESPACE='App\Logics\Api\V4'
```

### 1.4 路由配置
可配置通用路由，如下：
```php
Route::middleware([])
    ->namespace('Api\V4')
    ->group(function () {
        // 通用路由
        Route::get("{controller}/{action}/{id}", routeCommon());
        Route::get("{controller}/{action}", routeCommon());
    });

function routeCommon()
{
    $url = explode('/', request()->url());
    $action = array_pop($url);
    if (is_numeric($action)) {
        $action = array_pop($url);
    }
    $controller = 'Anchu\Cockpit\Controllers\CommonController';
    $action = \Lvzmen\Helper\iFormatHelper::camelize($action);
    $action = lcfirst($action);
    $url = $controller . '@' . $action;
    return $url;
}
```
> 配置通用路由的好处就是你不需要再为大多数的请求写路由和控制器，通用路由可以根据你的url直接请求到logic代码，提高开发速度，上述的通用路由实际的效果为：/api/secure/censor 可以直接执行 app/Logics/Secure/CensorLogics；

### 1.5 使用方法
在路径app下创建如下目录和文件：
```shell
├── app
│   ├── Decorators
│   │   └── Actions
│   │   └── Decorator.php
```
`Decorator.php`文件中存放通用数据处理方法:
```php
<?php

namespace App\Decorators;
use Anchu\Cockpit\Decorators\IDecorator;
use Anchu\Cockpit\Decorators\Decorator as Father;

/**
 * Class Decorator
 * @method self hello()
 * @package App\Decorators
 */
class Decorator extends Father
{
    public function test(): IDecorator
    {
        return $this;
    }
}
```

Actions目录放置业务相关的通用处理逻辑：
```php
<?php

namespace App\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;
use Anchu\Cockpit\Decorators\Actions\Action;

class Hello extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        return $this->decorator;
    }
}
```

有了上面的准备工作后，就可以创建Logic来使用Decorator装饰器带来的便利了：
```php
<?php

namespace App\Logics\Api\V4\Mushroom;

use Anchu\Cockpit\Logics\Logic;
use Illuminate\Http\Request;

class OutputLogic extends Logic
{
    public $api = 'ads_syj_clcz_y';
    public $apiToken = 'xxxx';
    public $method = 'get';
    public $url = 'http://api.com';

    public function run(Request $request)
    {
        return $this->decorator
            ->output()  // 这是通用业务逻辑处理模块，在/app/Decorators/Actions下面
            ->toFloat(['amount', 'value']) // 这是在/app/Decorator中定义的通用数据处理器
            ->all();
    }
}

```


## 2 工作台登录

### 2.1 简介
驾驶舱经常要使用到鉴权功能，需要跳转到工作台登录后拿到code，然后在驾驶舱调用登录接口，传递code进行鉴权。

### 2.2 准备条件
在工作台新增应用，并获取到app_key和app_secret；

### 2.3 配置如下
打开.env文件，加入如下配置i:
```shell
# 工作台地址及授权信息
PLATFORM_HOST=
PLATFORM_APP_KEY=
PLATFORM_APP_SECRET=
PLATFORM_LOGIN_EXPIRED=3600

# redis配置
REDIS_HOST=
REDIS_PASSWORD=
CACHE_DRIVER=redis
```

### 2.4 配置路由
`routes/api.php`：
```php
// 登录
Route::middleware([])
    ->namespace('Anchu\Cockpit\Controllers')
    ->group(function () {
        // 工作台登录
        Route::get('/platform/login', 'PlatformController@login');
    });

// 鉴权
Route::middleware(['jsc.auth'])
    ->namespace('App\Http\Controllers\Jsc')
    ->group(function () {
        // 首页
        Route::get('/index/summary', 'IndexController@summary');
});
```

### 2.5 配置中间件
`app/Http/Kernel.php`:
```php
protected $routeMiddleware = [
    // 自定义中间件
    'jsc.auth' => PlatformLoginAuth::class,
];
```

### 2.6 如何获取token
```shell
curl --location --request GET 'http://127.0.0.1:81/jsc/platform/login?code=4a00dd49ef8ef6eb1568bd4154c44e09' 
```
返回token及用户信息：
```json
{
    "code": 0,
    "msg": "success",
    "data": {
        "token": "00570f469daef6e9119f0c9ab21d20e0",
        "expired": "3600",
    }
}
```

### 2.7 如何使用token

```shell
curl --location --request GET 'http://127.0.0.1:81/jsc/tour/car?year=2022' \
--header 'Authorization: Bearer 00570f469daef6e9119f0c9ab21d20e0' \
--header 'Accept: */*' \
--header 'Host: 127.0.0.1:81' \
--header 'Connection: keep-alive'
```

## 3 乡村的大脑单点登录

### 3.1 简介

需要在浙江乡村大脑中配置应用，获取key和secret，然后需要配置服务器的白名单，才能使用。

### 3.2 配置如下

打开.env文件，加入如下配置i:

```shell
# 工作台地址及授权信息
XCDN_APP_KEY=
XCDN_APP_SECRET=
XCDN_LOGIN_EXPIRED=3600

# redis配置
REDIS_HOST=
REDIS_PASSWORD=
CACHE_DRIVER=redis
```

### 3.3 配置路由

`routes/api.php`：

```php
// 登录
Route::middleware([])
    ->namespace('Anchu\Cockpit\Controllers')
    ->group(function () {
        // 工作台登录
        Route::get('/xcdn/login', 'XcdnController@login');
    });

// 鉴权
Route::middleware(['jsc.auth'])
    ->namespace('App\Http\Controllers\Jsc')
    ->group(function () {
        // 首页
        Route::get('/index/summary', 'IndexController@summary');
});
```

### 3.4 配置中间件

`app/Http/Kernel.php`:

```php
protected $routeMiddleware = [
    // 自定义中间件
    'jsc.auth' => PlatformLoginAuth::class,
];
```

### 3.5 如何获取token

```shell
curl --location --request GET 'http://127.0.0.1:81/jsc/xcdn/login?code=drIBUNd0L2TyrpAl0b9LoTDh9P7ldZUFXnYzLWqgiFARv2MD0wWrY7KUsBK1' 
```

返回token及用户信息：

```json
{
  "code": 0,
  "msg": "success",
  "data": {
    "token": "00570f469daef6e9119f0c9ab21d20e0",
    "expired": "3600"
  }
}
```

### 3.6 如何使用token

```shell
curl --location --request GET 'http://127.0.0.1:81/jsc/tour/car?year=2022' \
--header 'Authorization: Bearer 00570f469daef6e9119f0c9ab21d20e0' \
--header 'Accept: */*' \
--header 'Host: 127.0.0.1:81' \
--header 'Connection: keep-alive'