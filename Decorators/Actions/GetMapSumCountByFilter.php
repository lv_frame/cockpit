<?php

namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

// 对地图进行信息统计
class GetMapSumCountByFilter extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $townName = $arguments[0] ?? '';
        $townCode = $arguments[1] ?? '';
        $villageName = $arguments[2] ?? '';
        $villageCode = $arguments[3] ?? '';
        $filterMap = $arguments[4] ?? [];

        return $this->decorator
            ->getMapListByFilter($filterMap)
            ->addColumns(['count' => 1])
            ->getMapSumWithFilterInBody($townName, $townCode, $villageName, $villageCode, 'count');
    }
}
