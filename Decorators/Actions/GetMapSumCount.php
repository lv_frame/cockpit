<?php


namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

// 对地图进行信息统计
class GetMapSumCount extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $code = $arguments[0] ?? '';
        $t_code = $arguments[1] ?? '';
        $v_code = $arguments[2] ?? '';
        $value = $arguments[3] ?? 'count';

        $this->decorator->data = array_map(function ($item) {
            $item['count'] = 1;
            return $item;
        }, $this->decorator->findByCode($code, $t_code, $v_code)->data);

        $column = $code == '' ? $t_code : $v_code;
        return $this->decorator->sum($column, $value);
    }
}
