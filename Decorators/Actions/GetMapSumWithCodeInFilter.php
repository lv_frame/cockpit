<?php


namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

// 对地图进行信息统计
class GetMapSumWithCodeInFilter extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $t_code = $arguments[0] ?? '';
        $v_code = $arguments[1] ?? '';

        $filter = request()->query('filter', '{}');
        $filter = json_decode($filter, 1);
        $code = $filter['code'] ?? '';
        return $this->decorator->getMapSumCount($code, $t_code, $v_code);
    }
}
