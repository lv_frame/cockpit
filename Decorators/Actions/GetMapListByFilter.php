<?php

namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;
use Lvzmen\Helper\iArrayHelper;

/**
 * 根据参数进行数据过滤，允许在过滤前修复异常的入参名称
 * Class GetMapListByFilter
 * @package App\Decorators\Actions
 */
class GetMapListByFilter extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $filterMap = $arguments[0] ?? [];

        $filter = request()->query('filter', '{}');
        $filter = json_decode($filter, 1) ?? [];

        if (!empty($filterMap)) {
            // town => town_name
            $filter = iArrayHelper::setKeys($filter, $filterMap);
        }

        return $this->decorator->filter($filter);
    }
}
