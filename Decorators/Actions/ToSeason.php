<?php


namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

// 截取到当前季度
class ToSeason extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $season = ceil(date('n') / 3); // 当前是第几季度
        $data = [];
        foreach ($this->decorator->data as $key => $item) {
            if ($key < $season) {
                $data[] = $item;
            }
        }
        $this->decorator->data = $data;
        return $this->decorator;
    }
}
