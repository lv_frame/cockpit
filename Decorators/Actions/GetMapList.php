<?php


namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

class GetMapList extends Action
{
    /**
     * @var Decorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $code = $arguments[0] ?? '';
        return $this->decorator->findByCode($code)->map([
            'villages_and_towns' => 'village',
            'kind' => 'production',
            'principal' => 'host',
            'tel' => 'mobile',
            'intro' => 'info',
        ])->unSensible(['mobile']);

    }
}
