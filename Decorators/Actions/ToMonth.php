<?php


namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

// 截取到当前月份
class ToMonth extends Action
{
    /**
     * @var Decorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $month = date('n'); // 当前是第几月
        $data = [];
        foreach ($this->decorator->data as $key => $item) {
            if ($key < $month) {
                $data[] = $item;
            }
        }
        $this->decorator->data = $data;
        return $this->decorator;
    }
}
