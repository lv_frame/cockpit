<?php


namespace Anchu\Cockpit\Decorators\Actions;


use Anchu\Cockpit\Decorators\Decorator;
use Lvzmen\Helper\iArrayHelper;

class Output extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $this->decorator->data = iArrayHelper::setKeys(
            $this->decorator->data,
            ['cl' => 'amount', 'cz' => 'value']
        );

        return $this->decorator->sortBy('year', SORT_ASC);
    }
}
