<?php


namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

abstract class Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function __construct(IDecorator $decorate)
    {
        $this->decorator = $decorate;
    }

    abstract public function run(array $arguments);
}
