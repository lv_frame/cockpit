<?php

namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

// 对地图进行信息统计
class GetMapSumWithFilterInQuery extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $townName = $arguments[0] ?? '';
        $townCode = $arguments[1] ?? '';
        $villageName = $arguments[2] ?? '';
        $villageCode = $arguments[3] ?? '';
        $sum = $arguments[4] ?? '';

        $filter = request()->query('filter', '{}');
        $filter = json_decode($filter, 1) ?? [];

        return $this->decorator
            ->filter($filter)
            ->getMapSumWithFilterInBody($townName, $townCode, $villageName, $villageCode, $sum);
    }
}
