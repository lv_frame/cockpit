<?php


namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

// 获取地图主体列表信息
class GetMapListWithCodeInFilter extends Action
{
    /**
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $t_code = $arguments[0] ?? '';
        $v_code = $arguments[1] ?? '';

        $filter = request()->query('filter', '{}');
        $filter = json_decode($filter, 1);
        $code = $filter['code'] ?? '';

        return $code == ''
            ?
            $this->decorator
            :
            $this->decorator->filter(['or' => [$t_code => $code, $v_code => $code]]);
    }
}
