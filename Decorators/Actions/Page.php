<?php

namespace Anchu\Cockpit\Decorators\Actions;

use Anchu\Cockpit\Decorators\IDecorator;

class Page extends Action
{
    /**
     * 数仓接口的分类数据整理封装
     * @var IDecorator
     */
    public $decorator;

    public function run($arguments): IDecorator
    {
        $page = request()->query('page', 1) * 1;
        $pageSize = request()->query('page_size', 10) * 1;
        $url = request()->url();
        $total = $this->decorator->data[1][0]['total'] ?? 0;
        $data = $this->decorator->data[0] ?? [];

        $pages = ceil($total / $pageSize);

        $this->decorator->data = [
            'data' => $data,
            'current_page' => $page,
            'per_page' => $pageSize,
            'last_page' => $pages,
            'first_page_url' => $url . '?page=' . 1,
            'last_page_url' => $url . '?page=' . $pages,
            'next_page_url' => $page == $pages ? null : ($url . '?page=' . ($page + 1)),
            'prev_page_url' => $page == 1 ? null : ($url . '?page=' . ($page - 1)),
            'path' => $url,
            'from' => $pageSize * ($page - 1) + 1,
            'to' => ($page * $pageSize >= $total) ? $total : ($page * $pageSize),
            'total' => $total,
        ];
        return $this->decorator;
    }
}
